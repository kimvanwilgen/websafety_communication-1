**Subject:** Safety first: take action to fix vulnerability at `{{Company}}`


Hi, `{{first_name}}`!

I’m a security researcher from the Netherlands.

Long story short: I found the security vulnerability on the `{{Company}}` website.

It's a public folder with unrestricted access.

I want to share my report with you, or some techie.

Best,

Max 

Security Researcher at WebSafety Ninja